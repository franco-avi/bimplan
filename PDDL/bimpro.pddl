(define (problem bimpro)
	(:domain bimplan)

	(:requirements :strips)

	(:objects	
					; Macchinari
					betoniera1 
					betoniera2 
					gru1
					
					; Squadre
					squadra1
					squadra2 
					
					; Elementi
					fondazionesx 
					fondazionedx 
					muro1 
					tetto1)

	(:init	

				; Tipizzazione degli oggetti
				(squadra squadra1)
				(squadra squadra2)
				(fond fondazionesx)
				(fond fondazionedx)
				(fondsx fondazionesx)
				(fonddx fondazionedx)
				(muro muro1)
				(tetto tetto1)

				; Assegnazione competenze utilizzo macchinari
				(usa squadra1 betoniera1)
				(usa squadra1 betoniera2)
				(usa squadra2 betoniera1)
				(usa squadra2 betoniera2)
				(usa squadra2 gru1)
	
				; Assegnazione funzionalità ai macchinari
				(getto betoniera1)
				(getto betoniera2)
				(posatura gru1)
	)

	(:goal	(and	(costruito fondazionesx)
						(costruito fondazionedx)
						(costruito muro1)
						(costruito tetto1))
	) 
)