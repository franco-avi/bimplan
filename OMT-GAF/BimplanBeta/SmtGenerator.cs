﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using System.Windows.Forms;
using System.Threading;
using GAF;
using GAF.Operators;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class SmtGenerator : IExternalCommand
    {
        IList<ElementId> prioritiesOrder;
        Graph g;
        DateTime start;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            /*SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Txt Files (*.txt)|*.txt";
            saveFileDialog.Title = "Save a txt File";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                BimplanCore.generateSMT(commandData.Application.ActiveUIDocument.Document, saveFileDialog.FileName);
            }*/

            g = new Graph(commandData.Application.ActiveUIDocument.Document);
            prioritiesOrder = new List<ElementId>();
            foreach (KeyValuePair<ElementId, Node> pair in g.getNodes())
            {
                prioritiesOrder.Add(pair.Key);
            }
       
            const int populationSize = 100;
            var population = new Population();
            var random = new Random();
            for (var p = 0; p < populationSize; p++)
            {
                var chromosome = new Chromosome();
                for (int i = 0; i < g.getNodes().Count; i++)
                {
                    chromosome.Add(new Gene(random.NextDouble()));            
                }
                population.Solutions.Add(chromosome);
            }

            var elite = new Elite(15);
            var crossover = new Crossover(0.85, true, CrossoverType.SinglePoint);

            var ga = new GeneticAlgorithm(population, delegate(Chromosome chromosome) {

                IDictionary<ElementId, double> priorities = new Dictionary<ElementId, double>();
                int index = 0;
                foreach (Gene gene in chromosome.Genes)
                {
                    priorities[prioritiesOrder.ElementAt(index++)] = gene.RealValue;
                }
                priorities[g.getRoot().getElementId()] = 1.1;
                priorities[g.getSink().getElementId()] = -1.1;

                IDictionary<Node, int> finishTimes = ScheduleEvaluator.getSchedule(g, priorities);
                return 1000.0 / finishTimes[g.getSink()];
            });

            //ga.OnGenerationComplete += ga_OnGenerationComplete;
            ga.OnRunComplete += ga_OnRunComplete;

            ga.Operators.Add(crossover);
            ga.Operators.Add(elite);

            start = DateTime.Now;
            ga.Run(delegate(Population pop, int currentGeneration, long currentEvaluation) {
                return currentGeneration >= 50;
            }); 

            return Autodesk.Revit.UI.Result.Succeeded;
        }

        private void ga_OnRunComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];

            IDictionary<ElementId, double> priorities = new Dictionary<ElementId, double>();
            int index = 0;
            foreach (Gene gene in fittest.Genes)
            {
                priorities[prioritiesOrder.ElementAt(index++)] = gene.RealValue;
            }
            priorities[g.getSink().getElementId()] = -2.0;

            IDictionary<Node, int> finishTimes = ScheduleEvaluator.getSchedule(g, priorities);
            string msgBox = "Start: " + start.ToLongTimeString() + " End: " + DateTime.Now.ToLongTimeString() + "\n";
            foreach (KeyValuePair<Node, int> pair in finishTimes)
            {
                msgBox += "el" + pair.Key.getElementId() + ": " + (pair.Value - pair.Key.getDuration()) + "";
            }
            MessageBox.Show(msgBox + "\nFitness: " + 1000.0 / finishTimes[g.getSink()]);
        }

        private void ga_OnGenerationComplete(object sender, GaEventArgs e)
        {
            if (e.Generation % 1 == 0)
            {
                string genes = "[";
                int i = 0;
                foreach (Gene gene in e.Population.GetTop(1)[0].Genes)
                {
                    genes += "el" + prioritiesOrder.ElementAt(i++) + ": " + gene.RealValue + ", ";
                }
                genes += "]";

                MessageBox.Show("Generation: " + e.Generation
                    + "\nTime: " + 1000.0 / e.Population.MaximumFitness
                    + "\nFitness: " + e.Population.MaximumFitness 
                    + "\nIndividuals: " + e.Population.PopulationSize
                    + "\nChromosome: " + genes);
            }
        }
    }
}
