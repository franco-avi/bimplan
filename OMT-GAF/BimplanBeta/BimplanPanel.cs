﻿using System;
using System.Reflection;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using Autodesk.Revit.UI.Selection;
using System.Windows.Forms;
using System.IO;

namespace Bimplan
{
    public class BimplanPanel : IExternalApplication
    {
        private static string programFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
        public static string assembly = Path.Combine(programFile, @"BimplanBeta\Bimplan.dll");
        public static string imgPath = Path.Combine(programFile, @"BimplanBeta\img\");
        public static string omtPath = Path.Combine(programFile, @"BimplanBeta\omt\");

        public Result OnStartup(UIControlledApplication app)
        {
            RibbonPanel panel = app.CreateRibbonPanel("Bimplan");

            // set dependencies
            PushButton pushButtonSetDep = panel.AddItem(new PushButtonData("setDep", "Set\ndependencies\n", assembly, "Bimplan.SetDependencies")) as PushButton;
            pushButtonSetDep.ToolTip = "Specify the structural dependencies for the elements.";
            pushButtonSetDep.LargeImage = new BitmapImage(new Uri(imgPath + "set.ico"));

            //eyeview
            PushButton pushButtonHighlight = panel.AddItem(new PushButtonData("eyeview", "Highlight\ndependencies\n", assembly, "Bimplan.HighlightDependencies")) as PushButton;
            pushButtonHighlight.ToolTip = "Activate the highlight mode, which can be used in order to see the structural dependencies of the elements.";
            pushButtonHighlight.LargeImage = new BitmapImage(new Uri(imgPath + "highlight.ico"));

            // tools
            PushButton pushButtonTools = panel.AddItem(new PushButtonData("buildingReq", "Building\nrequirements\n", assembly, "Bimplan.SetBuildingRequirements")) as PushButton;
            pushButtonTools.ToolTip = "Specify the duration, manpower and machinery needed for the building of the element.";
            pushButtonTools.LargeImage = new BitmapImage(new Uri(imgPath + "breq.ico"));

            // separator
            panel.AddSeparator();

            // project manpower
            PushButtonData pushButtonManpower = new PushButtonData("projManpower", "Project manpower", assembly, "Bimplan.ProjManpower");
            pushButtonManpower.ToolTip = "Specify the manpower available during the building";
            pushButtonManpower.Image = new BitmapImage(new Uri(imgPath + "manp.ico"));

            // project machinery
            PushButtonData pushButtonMachinery = new PushButtonData("projMachinery", "Project machinery", assembly, "Bimplan.ProjMachinery");
            pushButtonMachinery.ToolTip = "Specify the machinery available during the building";
            pushButtonMachinery.Image = new BitmapImage(new Uri(imgPath + "wrench.ico"));

            panel.AddStackedItems(pushButtonManpower, pushButtonMachinery);

            // generate
            PushButtonData csv = new PushButtonData("csv", "Csv file", assembly, "Bimplan.CsvGenerator");
            csv.ToolTip = "Generate csv file";
            csv.LargeImage = new BitmapImage(new Uri(imgPath + "csv.ico"));
            PushButtonData smt = new PushButtonData("smt", "Smt file", assembly, "Bimplan.SmtGenerator");
            smt.ToolTip = "Generate smt file";
            smt.LargeImage = new BitmapImage(new Uri(imgPath + "smt.ico"));

            SplitButtonData sbd3 = new SplitButtonData("splitButton1", "Split");
            SplitButton sb3 = panel.AddItem(sbd3) as SplitButton;
            sb3.AddPushButton(csv);
            sb3.AddPushButton(smt);

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }
    }  
}