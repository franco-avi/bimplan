﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bimplan
{
    public partial class BuildingReqForm : Form
    {
        public bool saved = false;
        public int duration;
        public IDictionary<string, int> newManp;
        public IDictionary<string, int> newMac;

        public BuildingReqForm(IDictionary<string, int> manp, IDictionary<string, int> mac, int duration)
        {
            InitializeComponent();
            this.ActiveControl = this.saveButton;

            manpowerGrid.EditingControlShowing += grid_EditControlShowing;
            machineryGrid.EditingControlShowing += grid_EditControlShowing;

            foreach (KeyValuePair<string, int> pair in manp)
            {
                manpowerGrid.Rows.Add(pair.Key, pair.Value.ToString());
            }
            foreach (KeyValuePair<string, int> pair in mac)
            {
                machineryGrid.Rows.Add(pair.Key, pair.Value.ToString());
            }
            durationUpDown.Value = duration;
        }

        
        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.saved = false;
            this.Close();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            this.saved = true;
            bool invalid = false;

            this.duration = (int)durationUpDown.Value;
            if (duration == -1)
            {
                invalid = true;
            }

            if (!invalid)
            {
                this.newManp = new Dictionary<string, int>();
                foreach (DataGridViewRow row in manpowerGrid.Rows)
                {
                    int amount = Int32.Parse(row.Cells[1].Value.ToString());
                    if (amount == -1)
                    {
                        invalid = true;
                        newManp.Clear();
                        break;
                    }
                    newManp[row.Cells[0].Value.ToString()] = amount;
                }
            }

            if (!invalid)
            {
                this.newMac = new Dictionary<string, int>();
                foreach (DataGridViewRow row in machineryGrid.Rows)
                {
                    int amount = Int32.Parse(row.Cells[1].Value.ToString());
                    if (amount == -1)
                    {
                        invalid = true;
                        newMac.Clear();
                        break;
                    }
                    newMac[row.Cells[0].Value.ToString()] = amount;
                }
            }

            if (invalid)
            {
                this.saved = false;
                TaskDialog infoDialog = new TaskDialog("Error");
                infoDialog.MainContent = "Some values are invalid. Please enter a positive integer number for amounts and duration.";
                infoDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                infoDialog.DefaultButton = TaskDialogResult.Ok;
                TaskDialogResult result = infoDialog.Show();
            } else
            {
                this.Close();
            }
        }

        private void grid_EditControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column2_KeyPress);
            if (((DataGridView)sender).CurrentCell.ColumnIndex == 1)
            {
                System.Windows.Forms.TextBox tb = e.Control as System.Windows.Forms.TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column2_KeyPress);
                }
            }
        }

        private void Column2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


    }
}
