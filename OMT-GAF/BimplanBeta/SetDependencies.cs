﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class SetDependencies : IExternalCommand
    {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData revit,
           ref string message, ElementSet elements)
        {
            if (Properties.Settings.Default.addElementsDialog)
            {
                TaskDialog elementDialog = new TaskDialog("Set dependencies");
                elementDialog.MainContent = "Select the elements for which you want to define the dependencies.";
                elementDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                elementDialog.DefaultButton = TaskDialogResult.Ok;
                elementDialog.VerificationText = "Do not show again";
                elementDialog.Show();
                Properties.Settings.Default["addElementsDialog"] = !elementDialog.WasVerificationChecked();
            }

            UIDocument uidoc = revit.Application.ActiveUIDocument;
            OverrideGraphicSettings greenSolidGs = this.createSolidGreenGraphic(uidoc);
            IDictionary<ElementId, OverrideGraphicSettings> oldGs = new Dictionary<ElementId, OverrideGraphicSettings>();
            IList<Reference> curr = null;

            try
            {
                using (Transaction t = new Transaction(uidoc.Document, "Highlight elements with green color"))
                {
                    t.Start();
                    curr = uidoc.Selection.PickObjects(ObjectType.Element, new SelectionFilter(new List<Reference>(), uidoc), "Select the elements for the dependency definitions.");
                    foreach (Reference currRef in curr)
                    {
                        ElementId id = currRef.ElementId;
                        oldGs[id] = uidoc.ActiveView.GetElementOverrides(id);
                        uidoc.ActiveView.SetElementOverrides(id, greenSolidGs);
                    }
                    t.Commit();
                }

                if (Properties.Settings.Default.addDepsDialog)
                {
                    TaskDialog depDialog = new TaskDialog("Set dependencies");
                    depDialog.MainContent = "Select the dependencies for the green elements.";
                    depDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                    depDialog.DefaultButton = TaskDialogResult.Ok;
                    depDialog.VerificationText = "Do not show again";
                    depDialog.Show();
                    Properties.Settings.Default["addDepsDialog"] = !depDialog.WasVerificationChecked();
                }

                using (Transaction t = new Transaction(uidoc.Document, "Save dependencies"))
                {
                    t.Start();
                    IList<Reference> newDeps = uidoc.Selection.PickObjects(ObjectType.Element, new SelectionFilter(curr, uidoc), "Select the dependencies for the green elements.");

                    foreach (Reference currRef in curr)
                    {
                        Element element = uidoc.Document.GetElement(currRef.ElementId);
                        Entity ent = element.GetEntity(SchemasManager.getElementSchema());
                        if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());

                        IList<ElementId> newDepsIds = new List<ElementId>();
                        foreach (Reference dep in newDeps)
                        {
                            newDepsIds.Add(dep.ElementId);
                        }

                        ent.Set<IList<ElementId>>("dependencies", newDepsIds);
                        element.SetEntity(ent);
                    }

                    foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                    {
                        uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                    }
                    t.Commit();
                }

                if (Properties.Settings.Default.depsSavedDialog)
                {
                    TaskDialog savedDialog = new TaskDialog("Dependencies saved");
                    savedDialog.MainContent = "The dependencies have been set correctly.";
                    savedDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                    savedDialog.DefaultButton = TaskDialogResult.Ok;
                    savedDialog.VerificationText = "Do not show again";
                    savedDialog.Show();
                    Properties.Settings.Default["depsSavedDialog"] = !savedDialog.WasVerificationChecked();
                }
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException)
            {
                using (Transaction t = new Transaction(uidoc.Document, "Gray out elements"))
                {
                    t.Start();
                    foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                    {
                        uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                    }
                    t.Commit();
                }
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }

        public class SelectionFilter : ISelectionFilter
        {
            IList<ElementId> notAllowed = new List<ElementId>();
            UIDocument uidoc;
            public SelectionFilter(IList<Reference> notAllowed, UIDocument uidoc)
            {
                foreach (Reference reference in notAllowed)
                {
                    this.notAllowed.Add(reference.ElementId);
                }
                this.uidoc = uidoc;
            }

            public bool AllowElement(Element element)
            {
                return (element.Category != null
                               && element.Parameters.Size > 0
                               && element.Category.HasMaterialQuantities
                               && !notAllowed.Contains(element.Id));
            }

            public bool AllowReference(Reference refer, XYZ point)
            {
                return false;
            }
        }

        private OverrideGraphicSettings createSolidGreenGraphic(UIDocument uidoc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(uidoc.Document);
            IEnumerator<Element> enumerator = collector.OfClass(typeof(FillPatternElement)).GetEnumerator();
            Element fp = null;
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Name.Equals("Solid fill") || enumerator.Current.Name.Equals("Riempimento solido"))
                {
                    fp = enumerator.Current;
                    continue;
                }
            }

            OverrideGraphicSettings res = new OverrideGraphicSettings();
            res = new OverrideGraphicSettings();
            res.SetProjectionFillPatternVisible(true);
            res.SetProjectionFillColor(new Color(5, 152, 7));
            res.SetProjectionFillPatternId(fp.Id);

            return res;
        }
    }
}
