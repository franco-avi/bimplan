﻿using Autodesk.Revit.UI;
using System;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using System.Windows.Forms;
using System.Collections.Generic;
using Autodesk.Revit.UI.Selection;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class HighlightDependencies : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            UIDocument uidoc = commandData.Application.ActiveUIDocument;
            OverrideGraphicSettings yellowSolidGs = createSolidGraphic(uidoc, new Color(249, 218, 31));
            OverrideGraphicSettings greenSolidGs = createSolidGraphic(uidoc, new Color(5, 152, 7));
            IDictionary<ElementId, OverrideGraphicSettings> oldGs = new Dictionary<ElementId, OverrideGraphicSettings>();

            if (Properties.Settings.Default.eyeviewDialog)
            {
                TaskDialog infoDialog = new TaskDialog("Activate eyeview");
                infoDialog.MainContent = "Select the element for which you want to see the structural dependencies. Press Esc in order to exit from the command."; 
                infoDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                infoDialog.DefaultButton = TaskDialogResult.Ok;
                infoDialog.VerificationText = "Do not show again";
                infoDialog.Show();
                Properties.Settings.Default["eyeviewDialog"] = !infoDialog.WasVerificationChecked();
            }

            try
            {
                Element element = null;
                while (true)
                {
                    Reference reference = uidoc.Selection.PickObject(ObjectType.Element, new SelectionFilter((element == null ? null : element.Id)), "Select the element for the eyeview. Press Esc in order to exit.");
                    element = uidoc.Document.GetElement(reference.ElementId);

                    using (Transaction t = new Transaction(uidoc.Document, "Highlight dependencies"))
                    {
                        t.Start();                     
                        foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                        {
                            uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                        }
                        oldGs.Clear();
                        
                        Entity ent = element.GetEntity(SchemasManager.getElementSchema());
                        if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());
                        IList<ElementId> deps = ent.Get<IList<ElementId>>("dependencies");

                        oldGs[element.Id] = uidoc.ActiveView.GetElementOverrides(element.Id);
                        uidoc.ActiveView.SetElementOverrides(element.Id, greenSolidGs);
                        foreach (ElementId id in deps)
                        {
                            oldGs[id] = uidoc.ActiveView.GetElementOverrides(id);
                            uidoc.ActiveView.SetElementOverrides(id, yellowSolidGs);
                        }
                        t.Commit();
                    }
                }
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException)
            {
                using (Transaction t = new Transaction(uidoc.Document, "Gray out dependencies"))
                {
                    t.Start();
                    foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                    {
                        uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                    }
                    t.Commit();
                }
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }

        private OverrideGraphicSettings createSolidGraphic(UIDocument uidoc, Color color)
        {
            FilteredElementCollector collector = new FilteredElementCollector(uidoc.Document);
            IEnumerator<Element> enumerator = collector.OfClass(typeof(FillPatternElement)).GetEnumerator();
            Element fp = null;
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Name.Equals("Solid fill") || enumerator.Current.Name.Equals("Riempimento solido"))
                {
                    fp = enumerator.Current;
                    continue;
                }
            }

            OverrideGraphicSettings res = new OverrideGraphicSettings();
            res = new OverrideGraphicSettings();
            res.SetProjectionFillPatternVisible(true);
            res.SetProjectionFillColor(color);
            res.SetProjectionFillPatternId(fp.Id);

            return res;
        }

        public class SelectionFilter : ISelectionFilter
        {
            private ElementId notAllowed;

            public SelectionFilter(ElementId notAllowed)
            {
                this.notAllowed = notAllowed;
            }

            public bool AllowElement(Element element)
            {
                return (element.Category != null
                               && element.Parameters.Size > 0
                               && element.Category.HasMaterialQuantities
                               && (notAllowed == null ? true : !element.Id.Equals(notAllowed)));
            }

            public bool AllowReference(Reference refer, XYZ point)
            {
                return false;
            }
        }
    }
}
