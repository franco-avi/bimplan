﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Bimplan
{
     
    public partial class ResourceForm : Form
    {
        public enum Mode { Manpower, Machinery };
        public Mode mode;
        public IDictionary<string, int> result = null;
        public bool saved = false;

        public ResourceForm(IDictionary<string, int> init, Mode mode)
        {
            InitializeComponent();
            this.ActiveControl = this.saveButton;
            resDataGrid.EditingControlShowing += grid_EditControlShowing;

            this.mode = mode;
            this.resourceLabel.Text = mode.ToString();
            this.Text = mode.ToString();

            foreach (KeyValuePair<string, int> pair in init)
            {
                this.resDataGrid.Rows.Add(pair.Key, pair.Value);
            }
        }
       
        private void saveButton_Click(object sender, EventArgs e)
        {
            saved = true;
            result = new Dictionary<string, int>();
            foreach (DataGridViewRow row in this.resDataGrid.Rows)
            {
                result[row.Cells[0].Value.ToString()] = Int32.Parse(row.Cells[1].Value.ToString());
            }
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            saved = false;
            this.Close();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddResourceForm form = new AddResourceForm(this.mode);
            form.ShowDialog();

            string name = form.name;
            int amount = form.amount;

            string res = name;
            int i = 2;
            while (exist(res))
            {
                res = name + (i++);
            }

            this.resDataGrid.Rows.Add(res, amount);
        }

        private bool exist(string name)
        {
            foreach (DataGridViewRow row in this.resDataGrid.Rows)
            {
                if (row.Cells[0].Value.ToString().Equals(name)) return true;
            }
            return false;
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            TaskDialog infoDialog = new TaskDialog("Are you sure?");
            infoDialog.MainContent = "Do you really want to delete this " + mode.ToString().ToLower() + "? All the building requirements will be updated accordingly.";
            infoDialog.CommonButtons = TaskDialogCommonButtons.Cancel | TaskDialogCommonButtons.Ok;
            infoDialog.DefaultButton = TaskDialogResult.Ok;
            TaskDialogResult result = infoDialog.Show();

            if (result == TaskDialogResult.Ok)
            {
                this.resDataGrid.Rows.RemoveAt(this.resDataGrid.SelectedRows[0].Index);
            }
        }

        private void grid_EditControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(Column2_KeyPress);
            if (((DataGridView)sender).CurrentCell.ColumnIndex == 1)
            {
                System.Windows.Forms.TextBox tb = e.Control as System.Windows.Forms.TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(Column2_KeyPress);
                }
            }
        }

        private void Column2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
