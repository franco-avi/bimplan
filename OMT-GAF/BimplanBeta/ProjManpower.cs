﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class ProjManpower : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Entity ent = SchemasManager.findDataStoreEntity(doc);
            IDictionary<string, int> init = ent.Get<IDictionary<string, int>>("manpower");

            ResourceForm form = new ResourceForm(init, ResourceForm.Mode.Manpower);
            form.ShowDialog();

            if (form.saved)
            {
                using (Transaction t = new Transaction(doc, "Save project manpower"))
                {
                    t.Start();
                    ent.Set<IDictionary<string, int>>("manpower", form.result);
                    SchemasManager.findDataStoreElement(doc).SetEntity(ent);

                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    collector
                        .WhereElementIsNotElementType()
                        .WhereElementIsViewIndependent()
                        .ToElements();

                    foreach (Element element in collector)
                    {
                        if (null != element.Category
                          && 0 < element.Parameters.Size
                          && (element.Category.HasMaterialQuantities))
                        {
                            Entity elementEnt = element.GetEntity(SchemasManager.getElementSchema());
                            if (!elementEnt.IsValid()) elementEnt = new Entity(SchemasManager.getElementSchema());

                            IDictionary<string, int> oldElementManpower = elementEnt.Get<IDictionary<string, int>>("manpower");
                            IDictionary<string, int> newElementManpower = new Dictionary<string, int>();

                            foreach (KeyValuePair<string, int> pair in form.result)
                            {
                                if (oldElementManpower.ContainsKey(pair.Key))
                                {
                                    newElementManpower[pair.Key] = oldElementManpower[pair.Key];
                                }
                                else
                                {
                                    newElementManpower[pair.Key] = 0;
                                }
                            }
                            elementEnt.Set<IDictionary<string, int>>("manpower",newElementManpower);
                            element.SetEntity(elementEnt);
                        }
                    }
                    t.Commit();
                }
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
