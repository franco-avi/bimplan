﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class ProjMachinery : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            Document doc = commandData.Application.ActiveUIDocument.Document;
            Entity ent = SchemasManager.findDataStoreEntity(doc);
            IDictionary<string, int> init = ent.Get<IDictionary<string, int>>("machinery");

            ResourceForm form = new ResourceForm(init, ResourceForm.Mode.Machinery);
            form.ShowDialog();

            if (form.saved)
            {

                using (Transaction t = new Transaction(doc, "Save project machinery"))
                {
                    t.Start();

                    ent.Set<IDictionary<string, int>>("machinery", form.result);
                    SchemasManager.findDataStoreElement(doc).SetEntity(ent);

                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    collector
                        .WhereElementIsNotElementType()
                        .WhereElementIsViewIndependent()
                        .ToElements();

                    foreach (Element element in collector)
                    {
                        if (null != element.Category
                          && 0 < element.Parameters.Size
                          && (element.Category.HasMaterialQuantities))
                        {
                            Entity elementEnt = element.GetEntity(SchemasManager.getElementSchema());
                            if (!elementEnt.IsValid()) elementEnt = new Entity(SchemasManager.getElementSchema());

                            IDictionary<string, int> oldElementMachinery = elementEnt.Get<IDictionary<string, int>>("machinery");
                            IDictionary<string, int> newElementMachinery = new Dictionary<string, int>();

                            foreach (KeyValuePair<string, int> pair in form.result)
                            {
                                if (oldElementMachinery.ContainsKey(pair.Key))
                                {
                                    newElementMachinery[pair.Key] = oldElementMachinery[pair.Key];
                                }
                                else
                                {
                                    newElementMachinery[pair.Key] = 0;
                                }
                            }
                            elementEnt.Set<IDictionary<string, int>>("machinery", newElementMachinery);
                            element.SetEntity(elementEnt);
                        }
                    }
                    t.Commit();
                }
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
