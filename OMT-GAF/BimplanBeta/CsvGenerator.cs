﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using System.Windows.Forms;
using System.Threading;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class CsvGenerator : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Csv Files (*.csv)|*.csv";
            saveFileDialog.Title = "Save a csv File";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                BimplanCore.generateCsv(commandData.Application.ActiveUIDocument.Document, saveFileDialog.FileName);
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
