﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bimplan
{
    public partial class AddResourceForm : Form
    {
        public string name;
        public int amount;
        public bool ok = false;

        public AddResourceForm(ResourceForm.Mode mode)
        {
            InitializeComponent();
            this.ActiveControl = this.nameTextBox;
            this.Text = "Add project " + mode.ToString().ToLower();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            ok = false;
            this.Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            ok = true;
            name = this.nameTextBox.Text;
            if (name == null || name.Trim().Equals("")) name = "unnamed";
            amount = (int) this.amountUpDown.Value;
            this.Close();
        }
    }
}
