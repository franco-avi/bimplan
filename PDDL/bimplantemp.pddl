(define (domain bimplantemp)
	(:requirements	:strips)

	(:predicates
			(squadra ?s)
			(fond ?f)
			(fondsx ?fsx)
			(fonddx ?fdx)
			(muro ?m)
			(tetto ?t)
			
			(costruito ?e)
			(libero ?e)

			(usa ?s ?m)

			(getto ?m)
			(posatura ?m)
	)

	(:durative-action costruisciFondsx

			:parameters (?s ?m ?f)

			:duration (= ?duration 1)
			
			:condition 	(and 	(over all (squadra ?s))
									(over all (getto ?m))
									(over all (usa ?s ?m))
									(over all (fondsx ?f))
									(at start (libero ?m))
									(at start (libero ?s))
							)
			
			:effect (and 	(at end (costruito ?f))
								(at start (not (libero ?m)))
								(at start (not (libero ?s)))
								(at end (libero ?m))
								(at end (libero ?s))
						)
	)

	(:durative-action costruisciFonddx

			:parameters (?s ?m ?f)

			:duration (= ?duration 1)
			
			:condition 	(and 	(over all (squadra ?s))
									(over all (getto ?m))
									(over all (usa ?s ?m))
									(over all (fonddx ?f))
									(at start (libero ?m))
									(at start (libero ?s))
							)
			
			:effect (and 	(at end (costruito ?f))
								(at start (not (libero ?m)))
								(at start (not (libero ?s)))
								(at end (libero ?m))
								(at end (libero ?s))
						)
	)

	
	(:durative-action costruiscoMuro 
			:parameters (?s ?m ?mu ?f1 ?f2)

			:duration (= ?duration 1)

			:condition (and		(over all (squadra ?s))
										(over all (posatura ?m))
										(over all (muro ?mu))
										(over all (fondsx ?f1))
										(over all (fonddx ?f2))
										(over all (usa ?s ?m))
										(over all (costruito ?f1))
										(over all (costruito ?f2))
										(at start (libero ?m))
										(at start (libero ?s))
								)
							
			:effect (and 	(at end (costruito ?mu))
								(at start (not (libero ?m)))
								(at start (not (libero ?s)))
								(at end (libero ?m))
								(at end (libero ?s))
						)
	)

	(:durative-action costruisciTetto
			:parameters (?s ?m ?t ?mu)

			:duration (= ?duration 1)

			:condition (and		(over all (squadra ?s))
										(over all (posatura ?m))
										(over all (muro ?mu))
										(over all (tetto ?t))
										(over all (usa ?s ?m))
										(over all (costruito ?mu))
										(at start (libero ?m))
										(at start (libero ?s))
								)

			:effect (and 	(at end (costruito ?t))
								(at start (not (libero ?m)))
								(at start (not (libero ?s)))
								(at end (libero ?m))
								(at end (libero ?s))
						)
	)
)