﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI.Selection;
using System.Windows.Forms;
using Autodesk.Revit.DB.ExtensibleStorage;

namespace Bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class SetBuildingRequirements : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            if (Properties.Settings.Default.setBuildingReqDialog)
            {
                TaskDialog infoDialog = new TaskDialog("Set building requirements");
                infoDialog.MainContent = "Select the elements for which you want to set the building requirements.";
                infoDialog.CommonButtons = TaskDialogCommonButtons.Ok;
                infoDialog.DefaultButton = TaskDialogResult.Ok;
                infoDialog.VerificationText = "Do not show again";
                infoDialog.Show();
                Properties.Settings.Default["setBuildingReqDialog"] = !infoDialog.WasVerificationChecked();
            }

            UIDocument uidoc = commandData.Application.ActiveUIDocument;
            OverrideGraphicSettings greenSolidGs = this.createSolidGreenGraphic(uidoc);
            IDictionary<ElementId, OverrideGraphicSettings> oldGs = new Dictionary<ElementId, OverrideGraphicSettings>();
            IList<Reference> curr = null;

            try
            {
                Entity dataStorageEnt;
                using (Transaction t = new Transaction(uidoc.Document, "Highlight elements with green color"))
                {
                    t.Start();
                    curr = uidoc.Selection.PickObjects(ObjectType.Element, new SelectionFilter(new List<Reference>(), uidoc), "Select the elements for the building requirements.");
                    foreach (Reference currRef in curr)
                    {
                        ElementId id = currRef.ElementId;
                        oldGs[id] = uidoc.ActiveView.GetElementOverrides(id);
                        uidoc.ActiveView.SetElementOverrides(id, greenSolidGs);
                    }
                    dataStorageEnt = SchemasManager.findDataStoreEntity(uidoc.Document);
                    t.Commit();
                }

                IDictionary<string, int> dataStorageManp = dataStorageEnt.Get<IDictionary<string, int>>("manpower");
                IDictionary<string, int> dataStorageMac = dataStorageEnt.Get<IDictionary<string, int>>("machinery");
                IDictionary<string, int> overallManp = new Dictionary<string, int>();
                IDictionary<string, int> overallMac = new Dictionary<string, int>();
                int overallDuration = 0;
               
                for (int i=0; i < curr.Count; i++)
                {
                    Element element = uidoc.Document.GetElement(curr[i].ElementId);
                    Entity elementEnt = element.GetEntity(SchemasManager.getElementSchema());
                    if (!elementEnt.IsValid()) elementEnt = new Entity(SchemasManager.getElementSchema());
                    IDictionary<string, int> elementManp = elementEnt.Get<IDictionary<string, int>>("manpower");
                    IDictionary<string, int> elementMac = elementEnt.Get<IDictionary<string, int>>("machinery");
                    int elementDuration = elementEnt.Get<int>("duration");

                    if (i == 0) {
                        overallDuration = elementDuration;
                    } else
                    {
                        overallDuration = (elementDuration != overallDuration ? -1 : overallDuration);
                    }
                    foreach (string key in dataStorageManp.Keys)
                    {
                        if (i == 0)
                        {
                            overallManp[key] = (elementManp.ContainsKey(key) ? elementManp[key] : 0);
                        } else
                        {
                            int amount = (elementManp.ContainsKey(key) ? elementManp[key] : 0);
                            overallManp[key] = (overallManp[key] != amount ? -1 : amount);
                        }
                    }
                    foreach (string key in dataStorageMac.Keys)
                    {
                        if (i == 0)
                        {
                            overallMac[key] = (elementMac.ContainsKey(key) ? elementMac[key] : 0);
                        }
                        else
                        {
                            int amount = (elementMac.ContainsKey(key) ? elementMac[key] : 0);
                            overallMac[key] = (overallMac[key] != amount ? -1 : amount);
                        }
                    }
                }              

                BuildingReqForm reqForm = new BuildingReqForm(overallManp, overallMac, overallDuration);
                reqForm.ShowDialog();

                using (Transaction t = new Transaction(uidoc.Document, "Gray out elements"))
                {
                    t.Start();
                    foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                    {
                        uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                    }
                    t.Commit();
                }

                using (Transaction t = new Transaction(uidoc.Document, "Save building requirements"))
                {
                    t.Start();
                    if (reqForm.saved)
                    {
                        foreach (Reference reference in curr)
                        {
                            Element element = uidoc.Document.GetElement(reference.ElementId);
                            Entity ent = element.GetEntity(SchemasManager.getElementSchema());
                            if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());

                            ent.Set<IDictionary<string, int>>("manpower", reqForm.newManp);
                            ent.Set<IDictionary<string, int>>("machinery", reqForm.newMac);
                            ent.Set<int>("duration", reqForm.duration);
                            element.SetEntity(ent);
                        }
                    }
                    t.Commit();
                }
            }
            catch (Autodesk.Revit.Exceptions.OperationCanceledException)
            {
                using (Transaction t = new Transaction(uidoc.Document, "Gray out elements"))
                {
                    t.Start();
                    foreach (KeyValuePair<ElementId, OverrideGraphicSettings> pair in oldGs)
                    {
                        uidoc.ActiveView.SetElementOverrides(pair.Key, pair.Value);
                    }
                    t.Commit();
                }
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }

        public class SelectionFilter : ISelectionFilter
        {
            IList<ElementId> notAllowed = new List<ElementId>();
            UIDocument uidoc;
            public SelectionFilter(IList<Reference> notAllowed, UIDocument uidoc)
            {
                foreach (Reference reference in notAllowed)
                {
                    this.notAllowed.Add(reference.ElementId);
                }
                this.uidoc = uidoc;
            }

            public bool AllowElement(Element element)
            {
                return (element.Category != null
                               && element.Parameters.Size > 0
                               && element.Category.HasMaterialQuantities
                               && !notAllowed.Contains(element.Id));
            }

            public bool AllowReference(Reference refer, XYZ point)
            {
                return false;
            }
        }

        private OverrideGraphicSettings createSolidGreenGraphic(UIDocument uidoc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(uidoc.Document);
            IEnumerator<Element> enumerator = collector.OfClass(typeof(FillPatternElement)).GetEnumerator();
            Element fp = null;
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.Name.Equals("Solid fill") || enumerator.Current.Name.Equals("Riempimento solido"))
                {
                    fp = enumerator.Current;
                    continue;
                }
            }

            OverrideGraphicSettings res = new OverrideGraphicSettings();
            res = new OverrideGraphicSettings();
            res.SetProjectionFillPatternVisible(true);
            res.SetProjectionFillColor(new Color(5, 152, 7));
            res.SetProjectionFillPatternId(fp.Id);

            return res;
        }
    }
}
