﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bimplan
{
    class BimplanCore
    {
        private static string getTime(int time, bool startMode)
        {
            DateTime now = DateTime.Now;
            GregorianCalendar cal = new GregorianCalendar();
            int tomorrowDay = cal.AddDays(now, 1).Day;
            int monthTomorrow = cal.AddDays(now, 1).Month;
            int yearTomorrow = cal.AddDays(now, 1).Year;

            DateTime startTime = new DateTime(yearTomorrow, monthTomorrow, tomorrowDay, 8, 0, 0);

            int daysToAdd = time / 8;
            int hoursToAdd = time % 8;
            if (hoursToAdd == 0 && time != 0)
            {
                daysToAdd = daysToAdd - 1;
                hoursToAdd = 8;
            }
            DateTime tmp = startTime;

            while (daysToAdd > 0)
            {
                tmp = cal.AddDays(tmp, 1);
                if (tmp.DayOfWeek == DayOfWeek.Saturday || tmp.DayOfWeek == DayOfWeek.Sunday)
                {
                    daysToAdd++;
                }
                daysToAdd--;
            }

            tmp = cal.AddHours(tmp, hoursToAdd);
            if (tmp.Hour == 16 && startMode)
            {
                tmp = cal.AddDays(tmp, 1);
                while (tmp.DayOfWeek == DayOfWeek.Saturday || tmp.DayOfWeek == DayOfWeek.Sunday)
                {
                    tmp = cal.AddDays(tmp, 1);
                }
                tmp = new DateTime(tmp.Year, tmp.Month, tmp.Day, 8, 0, 0);
            }

            return tmp.ToShortDateString() + " " + tmp.ToShortTimeString();
        }

        public static void generateCsv(Document doc, string path)
        {
            string smtpath = Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".smt2");
            generateInput(doc, smtpath);

            Process process = new Process();
            process.StartInfo.FileName = BimplanPanel.omtPath + "optimathsat.exe";
            process.StartInfo.Arguments = smtpath;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.Start();

            // create text file
            string outpath = Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".txt");
            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(outpath))
            {
                while (!process.StandardOutput.EndOfStream)
                {
                    outfile.WriteLine(process.StandardOutput.ReadLine());
                }
            }
            process.WaitForExit();
            File.Delete(smtpath);

            // parse csv
            string line;
            bool started = false;
            using (System.IO.StreamReader outfile = new System.IO.StreamReader(outpath))
            {
                using (System.IO.StreamWriter csvfile = new System.IO.StreamWriter(path))
                {
                    csvfile.Write("id;name;start time;end time;start time (clock);end time (clock);");
                    
                    // impose order
                    FilteredElementCollector collector = new FilteredElementCollector(doc);
                    var dataStorage = collector.OfClass(typeof(DataStorage)).FirstElement();
                    Entity entData = dataStorage.GetEntity(SchemasManager.getResourceSchema());
                    if (!entData.IsValid()) entData = new Entity(SchemasManager.getResourceSchema());

                    IDictionary<string, int> machinaryDoc = entData.Get<IDictionary<string, int>>("machinery");
                    IDictionary<string, int> manPowerDoc = entData.Get<IDictionary<string, int>>("manpower");
                    string[] resOrderMapping = new string[machinaryDoc.Keys.Count + manPowerDoc.Keys.Count];
                    int index = 0;
                    foreach (KeyValuePair<string, int> pair in machinaryDoc)
                    {
                        resOrderMapping[index++] = pair.Key + "-ma";
                        csvfile.Write(pair.Key + " (machinery);");
                    }
                    foreach (KeyValuePair<string, int> pair in manPowerDoc)
                    {
                        if (index == resOrderMapping.Length - 1)
                        {
                            csvfile.Write(pair.Key + " (manpower)\n");
                        }
                        else
                        {
                            csvfile.Write(pair.Key + " (manpower);");
                        }
                        resOrderMapping[index++] = pair.Key + "-mp";

                    }

                    while ((line = outfile.ReadLine()) != null)
                    {
                        if (started)
                        {
                            string trimmed = line.Replace("( (", "").Replace(") )", "").Replace(" ", ";");
                            string[] split = trimmed.Split(';');

                            Element element = doc.GetElement(new ElementId(Int32.Parse(split[0].Replace("el", ""))));
                            int startTime = Int32.Parse(split[1]);

                            Entity ent = element.GetEntity(SchemasManager.getElementSchema());
                            if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());

                            IDictionary<string, int> manPower = ent.Get<IDictionary<string, int>>("manpower");
                            IDictionary<string, int> machinary = ent.Get<IDictionary<string, int>>("machinery");

                            string elementId = element.Id.ToString();
                            string name = element.Name;
                            int endtime = ent.Get<int>("duration") + startTime;
                            string resources = "";

                            for (int i = 0; i < resOrderMapping.Length; i++)
                            {
                                string res = resOrderMapping[i];
                                if (res.EndsWith("-ma"))
                                {
                                    resources += machinary[res.Replace("-ma", "")].ToString() + ";";
                                }
                                else if (res.EndsWith("-mp"))
                                {
                                    resources += manPower[res.Replace("-mp", "")].ToString() + ";";
                                }
                            }
                            resources = resources.Substring(0, resources.Length - 1);

                            csvfile.WriteLine(elementId + ";" + name + ";" + startTime + ";" + endtime + ";" + getTime(startTime, true) + ";" + getTime(endtime, false) + ";" + resources);
                        }

                        if (line.Trim().Equals("sat")) started = true;
                    }
                }
            }
            File.Delete(outpath);
        }

        public static void generateSMT(Document doc, string path)
        {
            try {
                string temp = Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".smt2");
                generateInput(doc, temp);

                Process process = new Process();
                process.StartInfo.FileName = BimplanPanel.omtPath + "optimathsat.exe";
                process.StartInfo.Arguments = temp;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

                process.Start();
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
                {
                    while (!process.StandardOutput.EndOfStream)
                    {
                        file.WriteLine(process.StandardOutput.ReadLine());
                    }
                }
                process.WaitForExit();
                File.Delete(temp);
            } catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private static void generateInput(Document doc, string path)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
            {
                file.WriteLine("(set-option :produce-models true)");
                file.WriteLine();

                FilteredElementCollector collectorEl = new FilteredElementCollector(doc);
                collectorEl
                    .WhereElementIsNotElementType()
                    .WhereElementIsViewIndependent()
                    .ToElements();

                declareConstants(file, doc);
                startAtTimeZero(file, doc);
                addDependencies(file, doc);

                FilteredElementCollector collectorRes = new FilteredElementCollector(doc);
                var dataStorage = collectorRes.OfClass(typeof(DataStorage)).FirstElement();
                Entity entData = dataStorage.GetEntity(SchemasManager.getResourceSchema());
                if (!entData.IsValid()) entData = new Entity(SchemasManager.getResourceSchema());

                foreach (Element element in collectorEl)
                {
                    if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                    {
                        file.WriteLine("; check element el" + element.Id);
                        foreach (KeyValuePair<string, int> pair in entData.Get<IDictionary<string, int>>("machinery"))
                        {
                            addResourceConstraint(file, doc, element, pair.Key, "machinery", pair.Value);
                        }
                        foreach (KeyValuePair<string, int> pair in entData.Get<IDictionary<string, int>>("manpower"))
                        {
                            addResourceConstraint(file, doc, element, pair.Key, "manpower", pair.Value);
                        }
                        file.WriteLine();
                    }
                }

                file.WriteLine("(minimize sink)");
                getValues(file, doc);
            }
        }

        private static int getUpperBound(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();

            int upperbound = 0;
            foreach (Element element in collector)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    Entity ent = element.GetEntity(SchemasManager.getElementSchema());
                    if (ent.IsValid()) upperbound += ent.Get<int>("duration");
                }
            }
            return upperbound;
        }

        private static void declareConstants(System.IO.StreamWriter file, Document doc)
        {
            file.WriteLine("; print start time array");
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();

            foreach (Element element in collector)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    file.WriteLine("(declare-const el" + element.Id + " Int)");
                }
            }
            file.WriteLine("(declare-const sink Int)");
            file.WriteLine();
        }

        private static void startAtTimeZero(System.IO.StreamWriter file, Document doc)
        {
            file.WriteLine("; activities start at time 0 and before the upper bound");
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();
            int upperbound = getUpperBound(doc);

            foreach (Element element in collector)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    file.WriteLine("(assert (and (>= el" + element.Id + " 0) (<= el" + element.Id + " " + upperbound + ")))");
                }
            }
            file.WriteLine("(assert (>= sink 0))");
            file.WriteLine();
        }

        private static void addDependencies(System.IO.StreamWriter file, Document doc)
        {
            file.WriteLine("; dependencies");
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();

            foreach (Element element in collector)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    Schema schema = SchemasManager.getElementSchema();
                    Entity ent = element.GetEntity(schema);
                    if (ent.IsValid())
                    {
                        int duration = ent.Get<int>("duration");
                        IList<ElementId> dep = ent.Get<IList<ElementId>>("dependencies");
                        foreach (ElementId id in dep)
                        {
                            Element depEl = doc.GetElement(id);
                            Entity depEnt = depEl.GetEntity(schema);
                            int depDuration = 0;
                            if (depEnt.IsValid()) depDuration = depEnt.Get<int>("duration");

                            file.WriteLine("(assert (<= (+ el" + id + " " + depDuration + ") el" + element.Id + "))");
                        }

                        file.WriteLine("(assert (<= (+ el" + element.Id + " " + duration + ") sink))");
                    }
                }
            }
            file.WriteLine();
        }

        private static void addResourceConstraint(System.IO.StreamWriter file, Document doc, Element element, string resName, string resType, int resTotal)
        {
            file.WriteLine("(assert (<= (+");
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();

            foreach (Element other in collector)
            {
                if (null != other.Category && 0 < other.Parameters.Size && (other.Category.HasMaterialQuantities))
                {
                    Schema schema = SchemasManager.getElementSchema();
                    Entity ent = other.GetEntity(schema);
                    if (ent.IsValid())
                    {
                        int duration = ent.Get<int>("duration");
                        IDictionary<string, int> resMap = ent.Get<IDictionary<string, int>>(resType);
                        int resAmount = (resMap.ContainsKey(resName) ? resMap[resName] : 0);

                        file.WriteLine("\t(ite (and (>= el" + element.Id + " el" + other.Id + ") (< el" + element.Id + " (+ el" + other.Id + " " + duration + "))) " + resAmount + " 0)");
                    }
                }
            }
            file.WriteLine(") " + resTotal + "))");
        }

        private static void getValues(System.IO.StreamWriter file, Document doc)
        {
            file.WriteLine("(check-sat)");
            file.WriteLine("(set-model -1)");

            FilteredElementCollector collector = new FilteredElementCollector(doc);
            collector
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();
            foreach (Element element in collector)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    file.WriteLine("(get-value (el" + element.Id + "))");
                }
            }
        }
    }

}
