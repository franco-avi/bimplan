﻿using Autodesk.Revit.Creation;
using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bimplan
{
    class Graph
    {
        private Node root;
        private Node sink;
        private IDictionary<string, int> projectManpower;
        private IDictionary<string, int> projectMachinery;
        private IDictionary<ElementId, Node> nodes;

        public Graph(Autodesk.Revit.DB.Document doc)
        {
            nodes = new Dictionary<ElementId, Node>();
            root = new Node();
            sink = new Node(int.MaxValue);
           

            FilteredElementCollector collectorEl = new FilteredElementCollector(doc);
            collectorEl
                .WhereElementIsNotElementType()
                .WhereElementIsViewIndependent()
                .ToElements();
            foreach (Element element in collectorEl)
            {
                if (null != element.Category && 0 < element.Parameters.Size && (element.Category.HasMaterialQuantities))
                {
                    nodes[element.Id] = new Node(element.Id, doc);
                }
            }

            foreach (KeyValuePair<ElementId, Node> pair in nodes)
            {
                ElementId currId = pair.Key;
                Entity ent = doc.GetElement(currId).GetEntity(SchemasManager.getElementSchema());
                if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());

                IList<ElementId> dependencies = ent.Get<IList<ElementId>>("dependencies");
                foreach (ElementId dependency in dependencies)
                {
                    nodes[dependency].addEdge(nodes[currId]);
                    nodes[currId].addDependency(nodes[dependency]);
                }

                if (dependencies == null || dependencies.Count == 0)
                {
                    Node node = nodes[currId];
                    root.addEdge(node);

                    nodes[currId].addDependency(root);
                }
            }

            foreach (KeyValuePair<ElementId, Node> pair in nodes)
            {
                Node currNode = pair.Value;
                if (currNode.getEdges().Count == 0)
                {
                    currNode.addEdge(sink);
                    sink.addDependency(currNode);
                }
            }

            Entity dataStorageEntity = SchemasManager.findDataStoreEntity(doc);
            projectManpower = dataStorageEntity.Get<IDictionary<string, int>>("manpower");
            projectMachinery = dataStorageEntity.Get<IDictionary<string, int>>("machinery");

            nodes[root.getElementId()] = root;
            nodes[sink.getElementId()] = sink;
        }
        
        public Node getRoot()
        {
            return root;
        }

        public IDictionary<ElementId, Node> getNodes()
        {
            return nodes;
        }

        public Node getSink()
        {
            return sink;
        }

        public IDictionary<string, int> getProjectManpower()
        {
            return projectManpower;
        }

        public IDictionary<string, int> getProjectMachinery()
        {
            return projectMachinery;
        }

        public override string ToString()
        {
            return root.print();
        }
    }
}
