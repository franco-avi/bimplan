(define (problem bimprotemp)
	(:domain bimplantemp)

	(:objects	
					betoniera1 
					betoniera2 
					gru1
					
					squadra1
					squadra2 
					
					fondazionesx 
					fondazionedx 
					muro1 
					tetto1)

	(:init	

				(squadra squadra1)
				(squadra squadra2)
				(fond fondazionesx)
				(fond fondazionedx)
				(fondsx fondazionesx)
				(fonddx fondazionedx)
				(muro muro1)
				(tetto tetto1)

				(usa squadra1 betoniera1)
				(usa squadra1 betoniera2)
				(usa squadra2 betoniera1)
				(usa squadra2 betoniera2)
				(usa squadra1 gru1)
	
				(getto betoniera1)
				(getto betoniera2)
				(posatura gru1)

				(libero betoniera1)
				(libero betoniera2)
				(libero gru1)

				(libero squadra2)
				(libero squadra1)
	)

	(:goal	(and	(costruito fondazionesx)
						(costruito fondazionedx)
						(costruito muro1)
						(costruito tetto1))
	) 
)