(define (domain bimplan)
	(:requirements	:strips :negative-preconditions)

	(:predicates
			; Predicati per la tipizzazione degli oggetti
			(squadra ?s)
			(fond ?f)
			(fondsx ?fsx)
			(fonddx ?fdx)
			(muro ?m)
			(tetto ?t)
			
			; Indica se l'elemento è stato già costruito
			(costruito ?e)

			; Indica se la squadra s è in grado di usare il macchinario m
			(usa ?s ?m)

			; Funzionalità dei macchinari
			(getto ?m)
			(posatura ?m)
	)

	; Costruisci una singola fondazione (sx o dx)
	(:action costruisciSingolaFond

			:parameters (?s ?m ?f)
			
			:precondition 	(and 	(squadra ?s)
										(getto ?m)
										(usa ?s ?m)
										(fond ?f)
							 	 (not (costruito ?f)))
			
			:effect (costruito ?f)
	)

	; Costruisci due fondazioni in un colpo solo (problema parallelizzazione)
	(:action costruisciDoppiaFond
			:parameters (?s1 ?s2 ?m1 ?m2 ?f1 ?f2)
			
			:precondition 	(and 	(squadra ?s1)
										(squadra ?s2)
										(getto ?m1)
										(getto ?m2)
							    		(usa ?s1 ?m1)
										(usa ?s2 ?m2)
										(fondsx ?f1)
										(fonddx ?f2)
							   (not	(costruito ?f1))
							   (not	(costruito ?f2)))
			
			:effect (and	(costruito ?f1)
							   (costruito ?f2))
	)

	; Costrusci muro. Dipende da fondazionesx e fondazionedx
	(:action costruiscoMuro 
			:parameters (?s ?m ?mu ?f1 ?f2)

			:precondition (and	(squadra ?s)
										(posatura ?m)
										(muro ?mu)
										(fondsx ?f1)
										(fonddx ?f2)
										(usa ?s ?m)
										(costruito ?f1)
										(costruito ?f2)
							   (not	(costruito ?mu)))

			:effect (costruito ?mu)
	)

	; Costrusci tetto. Dipende da muro
	(:action costruisciTetto
			:parameters (?s ?m ?t ?mu)

			:precondition (and	(squadra ?s)
										(posatura ?m)
										(muro ?mu)
										(tetto ?t)
										(usa ?s ?m)
										(costruito ?mu)
					   		 (not (costruito ?t)))

			:effect (costruito ?t)
	)
)