﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bimplan
{
    class Node
    {
        private ElementId elementId;
        private IDictionary<string, int> machinery;
        private IDictionary<string, int> manpower;
        private int duration;
        private ISet<Node> edges;
        private ISet<Node> dependencies;

        public Node(ElementId elementId, Autodesk.Revit.DB.Document doc)
        {
            this.elementId = elementId;
            Entity ent = doc.GetElement(elementId).GetEntity(SchemasManager.getElementSchema());
            if (!ent.IsValid()) ent = new Entity(SchemasManager.getElementSchema());

            this.duration = ent.Get<int>("duration");
            this.machinery = ent.Get<IDictionary<string, int>>("machinery");
            this.manpower = ent.Get<IDictionary<string, int>>("manpower");
            this.edges = new HashSet<Node>();
            this.dependencies = new HashSet<Node>();
        }

        public Node()
        {
            this.elementId = new ElementId(0);
            this.machinery = new Dictionary<string, int>();
            this.manpower = new Dictionary<string, int>();
            this.duration = 0;
            this.edges = new HashSet<Node>();
            this.dependencies = new HashSet<Node>();
        }

        public Node(int id)
        {
            this.elementId = new ElementId(id);
            this.machinery = new Dictionary<string, int>();
            this.manpower = new Dictionary<string, int>();
            this.duration = 0;
            this.edges = new HashSet<Node>();
            this.dependencies = new HashSet<Node>();
        }
    

        public void addEdge(Node node)
        {
            this.edges.Add(node);
        }

        public void addDependency(Node node)
        {
            this.dependencies.Add(node);
        }

        public ElementId getElementId()
        {
            return this.elementId;
        }

        public int getDuration()
        {
            return duration;
        }

        public IDictionary<string, int> getManpower()
        {
            return this.manpower;
        }

        public IDictionary<string, int> getMachinery()
        {
            return this.machinery;
        }

        public ISet<Node> getEdges()
        {
            return this.edges;
        }

        public ISet<Node> getDependencies()
        {
            return this.dependencies;
        }

        public override bool Equals(Object other)
        {
            return this.elementId.Equals(((Node) other).getElementId());
        }

        public override int GetHashCode()
        {
            return this.elementId.GetHashCode();
        }

        public override string ToString()
        {
            return "Id: " + elementId;
        }

        public string print()
        {
            string result = this.elementId.ToString() + "{";
            foreach (Node node in edges)
            {
                result += node.print() + ", ";
            }
            result += "}";
            return result;
        }
    }
}
