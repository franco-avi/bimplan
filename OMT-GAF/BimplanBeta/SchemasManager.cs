﻿using Autodesk.Revit.DB;
using Autodesk.Revit.DB.ExtensibleStorage;
using System;

namespace Bimplan
{
    class SchemasManager
    {
        static Guid elementSchemaGUID = new Guid("97659eaa-ba4b-4d36-bbd7-d2b41867da00");
        static Guid resourceSchemaGUID = new Guid("bdaa8fa9-6640-41c4-8900-a18ee0cc53bd");

        private static Schema createElementSchema()
        {
            SchemaBuilder schemaBuilder = new SchemaBuilder(elementSchemaGUID);
            schemaBuilder.SetReadAccessLevel(AccessLevel.Public);
            schemaBuilder.SetWriteAccessLevel(AccessLevel.Public);
            schemaBuilder.SetSchemaName("ElementSchema");
            schemaBuilder.SetDocumentation("Schema for Bimplan resource and element constraints.");
            schemaBuilder.AddSimpleField("duration", typeof(int));
            schemaBuilder.AddArrayField("dependencies", typeof(ElementId));
            schemaBuilder.AddMapField("manpower", typeof(string), typeof(int));
            schemaBuilder.AddMapField("machinery", typeof(string), typeof(int));
            Schema schema = schemaBuilder.Finish();
            return schema;
        }

        private static Schema createResourceSchema()
        {
            SchemaBuilder schemaBuilder = new SchemaBuilder(resourceSchemaGUID);
            schemaBuilder.SetReadAccessLevel(AccessLevel.Public);
            schemaBuilder.SetWriteAccessLevel(AccessLevel.Public);
            schemaBuilder.SetSchemaName("ResourceSchema");
            schemaBuilder.SetDocumentation("Schema for Bimplan resource availability.");
            schemaBuilder.AddMapField("manpower", typeof(string), typeof(int));
            schemaBuilder.AddMapField("machinery", typeof(string), typeof(int));
            Schema schema = schemaBuilder.Finish();
            return schema;
        }

        public static Schema getElementSchema()
        {
            Schema elementSchema = Schema.Lookup(elementSchemaGUID);
            if (elementSchema == null)
            {
                elementSchema = createElementSchema();
            }
            return elementSchema;
        }

        public static Schema getResourceSchema()
        {
            Schema resourceSchema = Schema.Lookup(resourceSchemaGUID);
            if (resourceSchema == null)
            {
                resourceSchema = createResourceSchema();
            }
            return resourceSchema;
        }

        public static Entity findDataStoreEntity(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            var dataStorage = collector.OfClass(typeof(DataStorage)).FirstElement();
            if (dataStorage == null) dataStorage = DataStorage.Create(doc);

            Entity ent = dataStorage.GetEntity(SchemasManager.getResourceSchema());
            if (!ent.IsValid()) ent = new Entity(SchemasManager.getResourceSchema());

            return ent;
        }

        public static Element findDataStoreElement(Document doc)
        {
            FilteredElementCollector collector = new FilteredElementCollector(doc);
            Element dataStorage = collector.OfClass(typeof(DataStorage)).FirstElement();
            if (dataStorage == null) dataStorage = DataStorage.Create(doc);

            return dataStorage;
        }

    }
}
