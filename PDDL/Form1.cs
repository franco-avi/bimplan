﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bimplan
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        public Form1(Document doc)
        {
            InitializeComponent();

            // elementi
            FilteredElementCollector docCollector = new FilteredElementCollector(doc).WhereElementIsNotElementType();
            foreach (Element e in docCollector)
            {
                this.dataGridView1.Rows.Add(e.Id, e.Name, "", "");
            }

            //squadre
            this.dataGridView2.Rows.Add("squadra1", "getto;posatura");
            this.dataGridView2.Rows.Add("squadra2", "posatura");

            //macchinari
            this.dataGridView3.Rows.Add("betoniera1", "getto");
            this.dataGridView3.Rows.Add("gru1", "posatura");

            //salva
            button1.Click += new EventHandler(button_Save);
        }

        protected void button_Save(object sender, EventArgs args)
        {
            TaskDialog.Show("Attenzione","Funzionalità non ancora implementata!");
        }

    }
}
