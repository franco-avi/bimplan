﻿using System;
using Autodesk.Revit.UI;
using Autodesk.Revit.DB;
using System.Data;

namespace bimplan
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class Class1 : IExternalCommand
    {
        public Autodesk.Revit.UI.Result Execute(ExternalCommandData revit,
           ref string message, ElementSet elements)
        {

            UIDocument doc = revit.Application.ActiveUIDocument;
            if (doc == null)
            {
                TaskDialog.Show("Errore", "Nessun documento in uso.");
            } else
            {
                Form1 form1 = new Form1(doc.Document);
                form1.Show();
            }

            return Autodesk.Revit.UI.Result.Succeeded;
        }
    }
}
