﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Bimplan
{
    class ScheduleEvaluator
    {
        public static IDictionary<Node, int> getSchedule(Graph g, IDictionary<ElementId, double> priorities)
        {
            IDictionary<Node, int> finishTimes = new Dictionary<Node, int>();
            finishTimes[g.getRoot()] = 0;

            Node best = selectBest(g, finishTimes, priorities);
            while (best != null)
            {
                int bestTime = int.MaxValue;
                foreach (KeyValuePair<Node, int> pair in finishTimes)
                {
                    int startTime = pair.Value;
                    if (startTime < bestTime && trySchedule(startTime, best, g, finishTimes))
                    {
                        bestTime = startTime;
                    }
                }

                if (bestTime == int.MaxValue) throw new Exception("Impossible plan");
                finishTimes[best] = bestTime + best.getDuration();
                best = selectBest(g, finishTimes, priorities);
            }
            return finishTimes;
        }

        private static bool trySchedule(int time, Node node, Graph g, IDictionary<Node, int> finishTimes)
        {
            foreach (Node dependency in node.getDependencies())
            {
                if (!finishTimes.ContainsKey(dependency)) return false;
                if (time < finishTimes[dependency]) return false;
            }

            ISet<Node> active = new HashSet<Node>();
            foreach (KeyValuePair<Node, int> pair in finishTimes)
            {
                int finishTime = pair.Value;
                int startTime = finishTime - pair.Key.getDuration();

                if ((finishTime > time && finishTime <= time + node.getDuration()) || (startTime >= time && startTime < time + node.getDuration()))
                {
                    active.Add(pair.Key);
                }
            }

            IDictionary<string, int> machinery = new Dictionary<string, int>(g.getProjectMachinery());
            IDictionary<string, int> manpower = new Dictionary<string, int>(g.getProjectManpower());
            foreach (Node activeNode in active)
            {
                foreach (KeyValuePair<string, int> pair in activeNode.getMachinery())
                {
                    machinery[pair.Key] -= pair.Value;
                    if (machinery[pair.Key] < node.getMachinery()[pair.Key]) return false;
                }
                foreach (KeyValuePair<string, int> pair in activeNode.getManpower())
                {
                    manpower[pair.Key] -= pair.Value;
                    if (manpower[pair.Key] < node.getManpower()[pair.Key]) return false;
                }
            }
            return true;
        }

        private static Node selectBest(Graph g, IDictionary<Node, int> finishTimes, IDictionary<ElementId, double> priorities)
        {
            Node best = null;
            double bestPriority = double.MinValue;         
            foreach (KeyValuePair<ElementId, Node> pair in g.getNodes())
            {
                if (bestPriority < priorities[pair.Key] && !finishTimes.ContainsKey(pair.Value))
                {
                    bool depSat = true;
                    foreach (Node dependency in pair.Value.getDependencies())
                    {
                        depSat = depSat && finishTimes.ContainsKey(dependency);
                    }

                    if (depSat)
                    {
                        bestPriority = priorities[pair.Key];
                        best = pair.Value;
                    }
                }
            }
            return best;
        }
    }
}
